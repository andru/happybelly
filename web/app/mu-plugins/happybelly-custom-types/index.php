<?php
/*
Plugin Name: HappyBelly Custom Types
Description: Custom post types for HappyBelly
Author: Andrew Weir
*/

require_once 'acf.php';

add_action('init', function () {
    register_post_type('course', [
        'labels' => [
            'name' => 'Courses',
            'singular_name' => 'Course',
            'add_new' => 'Add New Course',
            'add_new_item' => 'Add New Course',
            'edit_item' => 'Edit Course',
            'new_item' => 'New Course',
            'all_items' => 'All Courses',
            'view_item' => 'View Course',
            'search_items' => 'Search Courses',
        ],
        'description' => 'A series of lessons designed to teach sets of concepts that enable students to master a specific topic.',
        'public' => true,
        'show_in_rest' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'supports' => ['title', 'thumbnail', 'custom-fields'],
        'show_in_graphql' => true,
        'rest_base' => 'courses',
		'graphql_single_name' => 'course',
		'graphql_plural_name' => 'courses',
    ]);

    register_post_type('lesson', [
        'labels' => [
            'name' => 'Lessons',
            'singular_name' => 'Lesson',
            'add_new' => 'Add New Lesson',
            'add_new_item' => 'Add New Lesson',
            'edit_item' => 'Edit Lesson',
            'new_item' => 'New Lesson',
            'all_items' => 'All Lessons',
            'view_item' => 'View Lesson',
            'search_items' => 'Search Lessons',
        ],
        'description' => 'A educational lesson, representing one section of a course.',
        'public' => true,
        'show_in_rest' => true,
        'rest_base' => 'lessons',
        'menu_position' => 5,
        'menu_icon' => 'dashicons-welcome-write-blog',
        'supports' => ['title', 'thumbnail', 'custom-fields'],
        'show_in_graphql' => true,
		'graphql_single_name' => 'lesson',
		'graphql_plural_name' => 'lessons',
    ]);
});

