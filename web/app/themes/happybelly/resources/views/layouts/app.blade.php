@php
$postType = get_post_type();

if ($postType == 'course') {
  $courseSlug = get_post()->post_name;
} else if ($postType == 'lesson') {
  $courseSlug = get_field('related')->post_name;
  $lessonSlug = get_post()->post_name;
} else {
  $courseSlug = 'todo else';
}

$courses = array_map(function ($course) {
  $item = [];
  $item['title'] = $course->post_title;
  $item['link'] = get_permalink($course->ID);
  $item['slug'] = $course->post_name;
  $item['lessons'] = array_map(function ($lesson) {
    return [
      'title' => $lesson->post_title,
      'link' => get_permalink($lesson->ID),
      'slug' => $lesson->post_name
    ];
  }, get_field('related', $course->ID));

  return $item;
}, get_posts([
  'post_type' => 'course',
  'post_status' => 'publish',
  'numberposts' => -1
]));

wp_reset_postdata();
@endphp
<!doctype html>
<html @php language_attributes() @endphp>
@include('partials.head')

<body @php body_class() @endphp>
  <div class="container-fluid p-0">
    @php do_action('get_header') @endphp
    @include('partials.header')

    <main>
      <!-- Module Dropdown -->
      <a class="nav-link dropdown-toggle d-lg-none" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
        aria-controls="collapseExample">Modules</a>
      <div class="collapse navbar-collapse pb-1" id="collapseExample">
        <ul class="navbar-nav module-nav">
          <li class="nav-item tab-1">
            <a class="nav-link" href="#">
              <img src="@asset('images/tab-menu-1.svg')" class="tab" alt="tab 1" />
              <img src="@asset('images/tab-menu-selected.svg')" class="select" alt="selected tab" />
              <span class="module-number">1</span>
              <span class="module-nav-title">Nutrition</span>
            </a>
          </li>
          <li class="nav-item tab-2">
            <a class="nav-link" alt="" href="#">
              <img src="@asset('images/tab-menu-2.svg')" class="tab" alt="tab 2" />
              <img src="@asset('images/tab-menu-selected.svg')" class="select" alt="selected tab" />
              <span class="module-number">2</span>
              <span class="module-nav-title">Emotional Health &amp; Wellbeing</span>
            </a>
          </li>
          <li class="nav-item tab-3 tab-selected">
            <a class="nav-link" alt="" href="#">
              <img src="@asset('images/tab-menu-3.svg')" class="tab" alt="tab 3" />
              <img src="@asset('images/tab-menu-selected.svg')" class="select" alt="selected tab" />
              <span class="module-number module-number-selected">3</span>
              <span class="module-nav-title">Exercise / Pelvic Floor Health</span>
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item tab-4">
            <a class="nav-link" href="#">
              <img src="@asset('images/tab-menu-4.svg')" class="tab" alt="tab 4" />
              <img src="@asset('images/tab-menu-selected.svg')" class="select" alt="selected tab" />
              <span class="module-number">4</span>
              <span class="module-nav-title">Environment / Body Care</span>
            </a>
          </li>
          <li class="nav-item tab-5">
            <a class="nav-link" href="#">
              <img src="@asset('images/tab-menu-5.svg')" class="tab" alt="tab 5" />
              <img src="@asset('images/tab-menu-selected.svg')" class="select" alt="selected tab" />
              <span class="module-number">5</span>
              <span class="module-nav-title">Birth</span>
            </a>
          </li>
          <li class="nav-item tab-6">
            <a class="nav-link" href="#">
              <img src="@asset('images/tab-menu-6.svg')" class="tab" alt="tab 6" />
              <img src="@asset('images/tab-menu-selected.svg')" class="select" alt="selected tab" />
              <span class="module-number">6</span>
              <span class="module-nav-title">Post-Partum Planning</span>
            </a>
          </li>
          <li class="nav-item tab-7">
            <a class="nav-link" href="#">
              <img src="@asset('images/tab-menu-7.svg')" class="tab" alt="tab 7" />
              <img src="@asset('images/tab-menu-selected.svg')" class="select" alt="selected tab" />
              <span class="module-number">7</span>
              <span class="module-nav-title">Tracking Your Pregnancy</span>
            </a>
          </li>
          <li class="nav-item tab-8">
            <a class="nav-link" href="#">
              <img src="@asset('images/tab-menu-8.svg')" class="tab" alt="tab 8" />
              <img src="@asset('images/tab-menu-selected.svg')" class="select" alt="selected tab" />
              <span class="module-number">8</span>
              <span class="module-nav-title">Cooking / Meal Planning</span>
            </a>
          </li>
        </ul>
      </div>

      <!-- Module Tab-->
      <div class="module-1 d-lg-none">
        <div class="module-1 d-flex flex-row justify-content-between open-tab">
          <span class="color d-inline-block w-100 d-flex align-items-center">
            <h3 class="module-title">
              <span class="module-number-open">1</span>Nutrition</h3>
          </span>
          <img src="@asset('images/tab-edge.svg')" />
        </div>
        <div class="color border-mod lesson-title">
          <p>Lesson: Fat Soluble Vitamins</p>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row">

          <!--  Module Sidebar Desktop     -->
          <div class="d-none d-lg-block col-lg-3 module-bg px-0">
            <ul class="nav flex-column module-nav">

              @foreach ($courses as $course)
                @php
                $index = $loop->index + 1;

                $selected = '';
                if ($course['slug'] == $courseSlug) {
                  $selected = 'selected';
                }

                @endphp
                <li class="nav-item lesson-title tab-{{ $index }} {{ $selected }}">
                  <a class="nav-link d-flex align-items-center" href="{{ $course['link'] }}">
                    <span class="module-nav-title mr-auto">{{ $course['title'] }}</span>
                    <img src="@asset("images/tab-menu-${index}.svg")" class="tab" alt="" />
                    <span class="module-number">{{ $index }}</span>
                  </a>
                  <ul>
                    @php
                    $selected = '';
                    if (!isset($lessonSlug)) {
                      $selected = 'selected';
                    }
                    @endphp
                    <a href="{{ $course['link'] }}" class="{{ $selected }}">
                      <li>Module overview</li>
                    </a>
                    @foreach ($course['lessons'] as $lesson)

                    @php
                    $selected = '';
                    if ($lesson['slug'] == $lessonSlug) {
                      $selected = 'selected';
                    }
                    @endphp
                    <a href="{{ $lesson['link'] }}" class="{{ $selected }}">
                      <li>{{ $lesson['title'] }}</li>
                    </a>
                    @endforeach
                  </ul>

                </li>
              @endforeach
            </ul>
          </div>

          <div class="col p-0 pl-lg-5 pr-lg-0">
            @yield('content')
          </div>
          <!-- end col dtcontentarea -->

        </div>
        <!-- end row -->
      </div>
      <!-- end container for content area -->

    </main>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	    crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	    crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
      crossorigin="anonymous"></script>
</body>

</html>
