@php
$post = get_post();
$lessons = get_field('related');
@endphp
<div class="p-3 p-sm-5 dtcontentarea">
  <h1>What you’ll learn in the {{ $post->post_title }} module</h1>
  <p>{!! $post->description !!}</p>
</div>


<!-- Lessons and Resources -->
<div class="container tab-content-area d-xl-none">
  <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active text-center" style="width: 45%; margin-right: 4px; " id="nav-home-tab" data-toggle="tab"
      href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Lessons</a>
    <a class="nav-item nav-link text-center" style="width: 45%; margin-left: 4px;" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
      role="tab" aria-controls="nav-profile" aria-selected="false">Resources</a>
  </div>
  <div class="tab-content" id="nav-tabContent">
    <!-- Lessons Tab -->
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
      <ul class="list-unstyled lesson-videos">
        <li class="media">
          <img class="mr-md-3 lesson-vid" src="@asset('images/video-thumb-placeholder.png')" alt="Generic placeholder image">
          <div class="media-body">
            <h2 class="mt-md-0 mb-1">List-based media object</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras bibendum arcu sed eros vestibulum,
              eget dapibus risus gravida. Etiam neque magna, tincidunt. </p>
          </div>
        </li>
        <li class="media">
          <img class="mr-md-3 lesson-vid" src="@asset('images/video-thumb-placeholder.png')" alt="Generic placeholder image">
          <div class="media-body">
            <h2 class="mt-md-0 mb-1">List-based media object</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras bibendum arcu sed eros vestibulum,
              eget dapibus risus gravida. Etiam neque magna, tincidunt. </p>
          </div>
        </li>
        <li class="media">
          <img class="mr-md-3 lesson-vid" src="@asset('images/video-thumb-placeholder.png')" alt="Generic placeholder image">
          <div class="media-body">
            <h2 class="mt-md-0 mb-1">List-based media object</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras bibendum arcu sed eros vestibulum,
              eget dapibus risus gravida. Etiam neque magna, tincidunt. </p>
          </div>
        </li>
      </ul>
    </div>

    <!-- Resources Tab -->
    <div class="tab-pane fade p-4 pt-5 p-sm-5" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
      <h3 class="pb-3">Work Sheets</h3>
      <ul class="list-unstyled">
        <li class="media">
          <img class="mr-3" src="@asset('images/icon-pdf.svg')" alt="Generic placeholder image">
          <div class="media-body">
            <h5 class="mt-0 mb-1">The work sheet for one of the lessons</h5>
          </div>
        </li>
        <li class="media my-3">
          <img class="mr-3" src="@asset('images/icon-pdf.svg')" alt="Generic placeholder image">
          <div class="media-body">
            <h5 class="mt-0 mb-1">The work sheet for one of the lessons</h5>
          </div>
        </li>
        <li class="media">
          <img class="mr-3" src="@asset('images/icon-pdf.svg')" alt="Generic placeholder image">
          <div class="media-body">
            <h5 class="mt-0 mb-1">The work sheet for one of the lessons</h5>
          </div>
        </li>
      </ul>
      <h3 class="pb-3 pt-4">Additional Resources</h3>
      <ul class="add-resources">
        <li>
          <a href="#">Additional resource link here</a>
        </li>
        <li>
          <a href="#">Additional resource link here</a>
        </li>
        <li>
          <a href="#">Additional resource link here</a>
        </li>
      </ul>
    </div>
  </div>
  <!-- tab-content END -->
</div>
<!-- tabbed-content END -->

<!-- Worksheets and Resources Desktop -->
<div class="container-fluid  d-none d-xl-block">
  <div class="row">
    <div class="col-8">

      <ul class="list-unstyled lesson-videos">
        @forelse ($lessons as $lesson)
        <li class="media">
          <a href="{{ get_permalink($lesson) }}">
            <div class="video-thumbnail">
              <img class="mr-3" style='width:248px' src="{{ grab_vimeo_thumbnail($lesson->video) }}" alt="Video Thumbnail">
            </div>
          </a>
          <div class="media-body">
            <h2 class="mt-0 mb-1"><a href="{{ get_permalink($lesson) }}">{{ $lesson->post_title }}</a></h2>
            <p>{!! wp_trim_words($lesson->description, 22) !!}</p>
          </div>
        </li>
        @empty
        <li class="media">
          <h2>No Lessons</h2>
        </li>
        @endforelse
      </ul>

    </div>

    <div class="col-4 worksheets">
      <h2 class="ws-header">Work Sheets &amp; Resources</h2>
      <div class="px-4 pt-3">
        <ul class="list-unstyled">
          <li class="media">
            <img class="mr-3" src="@asset('images/icon-pdf.svg')" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1">The work sheet for one of the lessons</h5>
            </div>
          </li>
          <li class="media my-3">
            <img class="mr-3" src="@asset('images/icon-pdf.svg')" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1">The work sheet for one of the lessons</h5>
            </div>
          </li>
          <li class="media">
            <img class="mr-3" src="@asset('images/icon-pdf.svg')" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1">The work sheet for one of the lessons</h5>
            </div>
          </li>
        </ul>
        <h3 class="text-center py-3">Additional Resources</h3>
        <ul class="add-resources">
          <li>
            <a href="#">Additional resource link here</a>
          </li>
          <li>
            <a href="#">Additional resource link here</a>
          </li>
          <li>
            <a href="#">Additional resource link here</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
