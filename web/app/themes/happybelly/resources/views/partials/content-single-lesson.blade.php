<div class="lesson-header-img"></div>
<div class="p-3 p-sm-5 dtcontentarea">
  <h1>{{ $post->post_title }}</h1>
  <p>{!! $post->description !!}</p>
  @php
      preg_match('/\/(\d+)/', $post->video, $match);
      $videoId = $match[1];
  @endphp
  <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; margin-top: 40px } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/{{ $videoId }}' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
</div>


<!-- Lessons and Resources -->
<div class="tab-content-area p-3 p-sm-5 dtcontentarea ">

  <div class="nav nav-tabs justify-content-center justify-content-md-start" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active text-center ml-md-2" style="margin-right: 4px; " id="nav-transcript-tab" data-toggle="tab"
      href="#nav-transcript" role="tab" aria-controls="nav-transcript" aria-selected="true">Transcript</a>
    <a class="nav-item nav-link text-center" style="margin-right: 4px; " id="nav-audio-tab" data-toggle="tab" href="#nav-audio"
      role="tab" aria-controls="nav-audio" aria-selected="false">Audio</a>
    <a class="nav-item nav-link text-center" style="margin-right: 4px; " id="nav-worksheets-tab" data-toggle="tab" href="#nav-worksheets"
      role="tab" aria-controls="nav-worksheets" aria-selected="false">Worksheets</a>
    <a class="nav-item nav-link text-center" style="margin-left: 4px;" id="nav-resources-tab" data-toggle="tab" href="#nav-resources"
      role="tab" aria-controls="nav-resources" aria-selected="false">Additional Resources</a>
  </div>

  <div class="tab-content" id="nav-tabContent">

    <!-- Transcripts Tab -->
    <div class="tab-pane fade show active p-4 pt-5 p-sm-5" id="nav-transcript" role="tabpanel" aria-labelledby="nav-transcript-tab">
      <div style='font-family: "Lucida Console", Monaco, monospace'>
          {{ $post->transcript }}
      </div>
    </div>


    <!-- Audio Tab -->
    <div class="tab-pane fade p-4 pt-5 p-sm-5" id="nav-audio" role="tabpanel" aria-labelledby="nav-audio-tab">
      <audio src="{{ get_field('audio')['url'] }}" controls></audio>
    </div>


    <!-- Worksheets Tab -->
    <div class="tab-pane fade p-4 pt-5 p-sm-5" id="nav-worksheets" role="tabpanel" aria-labelledby="nav-worksheets-tab">
      <ul class="list-unstyled">
          @forelse (get_field('worksheets') as $worksheet)
            <li class="media my-3">
              <img class="mr-3" src="@asset('images/icon-pdf.svg')" alt="Generic placeholder image">
              <div class="media-body">
                  <a href="{{ $worksheet['pdf']['url'] }}" title="Download Worksheet">
                    <h5 class="mt-0 mb-1">{{ $worksheet['worksheet_title'] }}</h5>
                  </a>
              </div>
            </li>
          @empty
            <li>No Worksheets</li>
          @endforelse
      </ul>
    </div>


    <!-- Resources Tab -->
    <div class="tab-pane fade p-4 pt-5 p-sm-5" id="nav-resources" role="tabpanel" aria-labelledby="nav-resources-tab">
      <h3 class="pb-3">Additional Resources</h3>
      <ul class="add-resources">
        @forelse (get_field('resources') as $resource)
          <li>
            <a href="{{ $resource['resource_url'] }}">{{ $resource['resource_name'] }}</a>
          </li>
        @empty
            <li>No Resource</li>
        @endforelse
      </ul>
    </div>

  </div>
  <!-- tab-content END -->
</div>
<!-- tabbed-content END -->
