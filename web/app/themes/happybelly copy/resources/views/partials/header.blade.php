<!-- Header -->
<header>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light mainnav">
    <a class="navbar-brand" href="/">
      <img src="@asset('images/logo.png')" alt="Happy Belly, A Guide To A Healthy Pregnancy And A Thriving Baby" class="logo-nav" />
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
      aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse d-none-lg justify-content-end" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link ml-4" href="#">Members
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link ml-4" href="#">Account</a>
        </li>
        <li class="nav-item">
          <a class="nav-link ml-4" href="#">Log Out</a>
        </li>
      </ul>
    </div>
  </nav>
</header>
