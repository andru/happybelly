<?php

namespace App;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public static function getCourses() {
        return get_posts(['post_type' => 'course']);
    }
}
